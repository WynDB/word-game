import { Component, OnInit } from '@angular/core'
import { Guess } from '../guess-row/guess-row.component'
import * as randomWords from "random-words"
import axios from 'axios'
import { MatSnackBar } from '@angular/material/snack-bar'

@Component({
  selector: 'app-guess-body',
  templateUrl: './guess-body.component.html',
  styleUrls: ['./guess-body.component.scss'],
  host: {
    class: "component-host"
  }
})
export class GuessBodyComponent implements OnInit {
  wordnikApiKey: string = ""
  showInfo: boolean = false
  showSettings: boolean = false

  letters: { letter: string, out?: boolean }[] = []
  total: { grandTotal: number, totalCorrect: number, guessCounts: number[] } = {
    grandTotal: 0,
    totalCorrect: 0,
    guessCounts: []
  }

  guesses: Guess[] = [{
    guess: "",
    submitted: false,
    indicesOutOfOrder: [],
    indicesCorrect: []
  }, {
    guess: "",
    submitted: false,
    indicesOutOfOrder: [],
    indicesCorrect: []
  }, {
    guess: "",
    submitted: false,
    indicesOutOfOrder: [],
    indicesCorrect: []
  }, {
    guess: "",
    submitted: false,
    indicesOutOfOrder: [],
    indicesCorrect: []
  }, {
    guess: "",
    submitted: false,
    indicesOutOfOrder: [],
    indicesCorrect: []
  }, {
    guess: "",
    submitted: false,
    indicesOutOfOrder: [],
    indicesCorrect: []
  }]

  previousWords: string[] = []
  currentWord: string = ""

  constructor(private snackBar: MatSnackBar) {
    while (this.letters.length < 26) {
      this.letters.push({ letter: String.fromCharCode(65 + this.letters.length) })
    }
  }

  ngOnInit(): void {
    this.getNewWord()
  }

  getNewWord() {
    let word = randomWords({ min: 1, max: 1, maxLength: 5 })[0]
    while (word.length < 5 || this.previousWords.indexOf(word) >= 0) {
      word = randomWords({ min: 1, max: 1, maxLength: 5 })[0]
    }

    this.currentWord = word.toUpperCase()
  }

  onKeyPress(event: KeyboardEvent) {
    if (event.key === "Backspace") {
      this.removeLetter()
      return
    }

    if (event.key === "Enter") {
      this.submit()
      return
    }

    if (event.key.length > 1) return

    const checkKey = /[A-z]/.exec(event.key)
    if (!checkKey?.length) return

    this.addLetter(event.key.toUpperCase())
  }

  addLetter(letter: string) {
    for (let guess of this.guesses) {
      if (guess.submitted) continue
      if (guess.guess.length >= 5) return

      guess.guess += letter
      return
    }
  }

  removeLetter() {
    for (let guess of this.guesses) {
      if (guess.submitted) continue

      guess.guess = guess.guess.substring(0, guess.guess.length - 1)
    }
  }

  //Just reset guesses, not totals
  softReset() {
    this.letters.forEach(l => l.out = false)
    this.previousWords.push(this.currentWord)
    this.getNewWord()

    for (let guess of this.guesses) {
      guess.guess = ""
      guess.indicesCorrect = []
      guess.indicesOutOfOrder = []
      guess.submitted = false
    }
  }

  reset() {
    this.softReset()

    this.total = {
      grandTotal: 0,
      totalCorrect: 0,
      guessCounts: []
    }
  }

  async submit() {
    for (let guess of this.guesses) {
      if (guess.submitted) continue
      if (guess.guess.length < 5) return

      await this.checkGuess(guess)

      if (guess.indicesCorrect.length == 5) {
        //We succeeded!
        this.completeSuccess()
        return
      }

      //We have not succeeded, yet, check if this is the last guess allowed and fail if so
      if (this.guesses.indexOf(guess) == this.guesses.length-1) this.completeFail()
    }
  }

  getAverageGuessCount() {
    if (!this.total.guessCounts.length) return 0
    return this.total.guessCounts.reduce((a, b) => a + b) / this.total.guessCounts.length
  }

  async checkWordExists(guess: Guess) {
    const result = await new Promise((res, rej) => {
      axios.get(`https://api.wordnik.com/v4/word.json/${guess.guess.toLowerCase()}/definitions?limit=200&includeRelated=false&useCanonical=false&includeTags=false&api_key=${this.wordnikApiKey}`)
        .then(r => res(200))
        .catch(err => res(err.response.status))
    })

    return result
  }

  async checkGuess(guess: Guess) {
    //Check word submitted is actually a word (oinly if the user has supplied an API key for Wordnik)
    if (this.wordnikApiKey) {
      const checkResult = await this.checkWordExists(guess)
      if (checkResult == 404) {
        this.wordDoesNotExist(guess)
        return
      }

      if (checkResult === 401) {
        this.invalidApiKey()
        return
      }
    }

    guess.submitted = true

    for (let i = 0; i < guess.guess.length; i++) {
      const guessChar = guess.guess[i]

      if (this.currentWord[i] == guessChar) {
        guess.indicesCorrect.push(i)
        continue
      }

      if (this.currentWord.indexOf(guessChar) >= 0) {
        guess.indicesOutOfOrder.push(i)
        continue
      }

      const letterStored = this.letters.find(l => l.letter === guessChar)
      if (letterStored) letterStored.out = true
    }
  }

  completeSuccess() {
    this.total.grandTotal++
    this.total.totalCorrect++
    this.total.guessCounts.push(this.guesses.filter(g => g.submitted).length)

    this.snackBar.open(`Correct! Well done!`, "Close", { verticalPosition: "top", horizontalPosition: "center", duration: 5000 })

    this.softReset()
  }

  completeFail() {
    this.total.grandTotal++

    this.snackBar.open(`The word was "${this.currentWord}"`, "Close", { verticalPosition: "top", horizontalPosition: "center", duration: 5000 })

    this.softReset()
  }

  wordDoesNotExist(guess: Guess) {
    guess.guess = ""

    this.snackBar.open(`Word does not exist!`, "Close", { verticalPosition: "top", horizontalPosition: "center", duration: 2000 })
  }

  invalidApiKey() {
    this.snackBar.open(`The supplied Wordnik API Key is invalid.`, "Close", { verticalPosition: "top", horizontalPosition: "center", duration: 2000 })
  }

  toggleInfo() {
    this.showInfo = !this.showInfo
  }

  toggleSettings() {
    this.showSettings = !this.showSettings
  }
}
