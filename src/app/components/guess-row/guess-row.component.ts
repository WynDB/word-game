import { Component, Input, OnInit } from '@angular/core';

export interface Guess {
  guess: string
  submitted: boolean,
  indicesCorrect: number[],
  indicesOutOfOrder: number[]
}

@Component({
  selector: 'app-guess-row',
  templateUrl: './guess-row.component.html',
  styleUrls: ['./guess-row.component.scss']
})
export class GuessRowComponent implements OnInit {
  @Input()
  guess?: Guess

  constructor() { }

  ngOnInit(): void {
  }

  indexIsCorrect(i: number) {
    if (!this.guess?.submitted) return false
    return this.guess.indicesCorrect.indexOf(i) >= 0
  }

  indexIsIncorrect(i: number) {
    if (!this.guess?.submitted) return false
    return !(this.indexIsCorrect(i) || this.indexIsOutOfOrder(i))
  }

  indexIsOutOfOrder(i: number) {
    if (!this.guess?.submitted) return false
    return this.guess.indicesOutOfOrder.indexOf(i) >= 0
  }
}
